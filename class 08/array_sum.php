<?php
$a = array(2, 4, 6, 8);
echo nl2br("sum(a) = " . array_sum($a) . "\n");

$b = array("a" => 1.2, "b" => 2.3, "c" => 3.4);
echo nl2br("sum(b) = " . array_sum($b) . "\n");