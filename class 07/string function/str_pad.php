<?php

$input = "MAAB";
echo str_pad($input, 10)."<br/>";                      // OUTPUT "MAAB     "
echo str_pad($input,  6, "___")."<br/>";  			   // OUTPUT "MAAB_"
echo str_pad($input, 10, "-=", STR_PAD_LEFT)."<br/>";  // OUTPUT "-=-=-MAAB"
echo str_pad($input, 10, "_", STR_PAD_BOTH)."<br/>";   // OUTPUT "__MAAB___"          
echo str_pad($input,  3, "*")."<br/>";                 // OUTPUT "MAAB"