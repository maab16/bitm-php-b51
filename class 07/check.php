<?php
session_start();

if (isset($_POST['even_odd'])) {
	$number = isset($_POST['number']) ? $_POST['number'] : "" ;
	if (!empty($number)) {
		if (($number%2) == 0) {
			
			$_SESSION['number'] = "{$number} is Even";
		}else{

			$_SESSION['number'] = "{$number} is Odd";
		}
	}else{
		$_SESSION['number'] = "Please Enter Number";
	}

	header('Location: even.php');
}
