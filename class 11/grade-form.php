<?php
	session_start();
	$mathErr=$englishErr=$banglaErr=$physicsErr="";
	if (isset($_SESSION['math'])) {
		$mathErr = $_SESSION['math'];
	}
	if (isset($_SESSION['english'])) {
		$englishErr = $_SESSION['english'];
	}
	if (isset($_SESSION['bangla'])) {
		$banglaErr = $_SESSION['bangla'];
	}
	if (isset($_SESSION['physics'])) {
		$physicsErr = $_SESSION['physics'];
	}

?>
<form action="grade.php" method="post">
	<label>Math : </label>
	<input type="number" name="math" placeholder="Enter Math Number"><br/> <?= $mathErr ?>
	<label>English : </label>
	<input type="number" name="english" placeholder="English Number"><br/><?= $englishErr ?>
	<label>Bangla : </label>
	<input type="number" name="bangla" placeholder="Bangla Number"><br/><?= $banglaErr ?>
	<label>Psysics : </label>
	<input type="number" name="physics" placeholder="Physics Number"><br/><?= $physicsErr ?>
	<input type="submit" name="grade" value="Result">
</form>
<?php
	session_destroy();

?>