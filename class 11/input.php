<?php

class Input{

	public $firstNumber;
	public $secondNumber;

	public function add(){

		return $this->firstNumber + $this->secondNumber;

	}
}

$input = new Input();

if(isset($_POST['send'])){

	$f_num = $_POST['f_num'];
	$s_num = $_POST['s_num'];
	$input->firstNumber = $f_num;
	$input->secondNumber = $s_num;

	echo $input->add();
}