<?php

class Input{

	public $firstNumber;
	public $secondNumber;

	public function add(){

		return $this->firstNumber + $this->secondNumber;

	}
	public function sub(){

		return $this->firstNumber - $this->secondNumber;

	}
	public function multi(){

		return $this->firstNumber * $this->secondNumber;

	}
	public function divide(){

		return ($this->secondNumber == 0) ? "Infinite" : $this->firstNumber / $this->secondNumber;

	}
}

$input = new Input();

if (isset($_POST['f_num']) && !empty($_POST['f_num'])) {
	$input->firstNumber = $_POST['f_num'];
	
}else{
	$input->firstNumber = 0;
}

if(isset($_POST['s_num']) && !empty($_POST['s_num'])){
	$input->secondNumber = $_POST['s_num'];
}else{

	$input->secondNumber = 0;
}

if(isset($_POST['add'])){

	echo $input->add();
}else if(isset($_POST['sub'])){

	echo $input->sub();
}else if(isset($_POST['multi'])){

	echo $input->multi();
}else if(isset($_POST['divide'])){

	echo $input->divide();
}