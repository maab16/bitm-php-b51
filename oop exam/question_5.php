<?php

interface User
{

}

trait Writing
{
	abstract public function writeContent();
}

class Author implements User
{
	use Writing;
	public function writeContent(){
		return "Author, please start typing an article...";
	}
}

class Commentator implements User
{
	use Writing;
	public function writeContent(){
		return "Commentator, please start typing your comment...";
	}
}

class Viewer implements User
{
	
}

//Instantiate Author
$author = new Author();
echo $author->writeContent()."<br>";
//Instantiate Commentator
$commentator = new Commentator();
echo $commentator->writeContent()."<br>";