<?php

class User
{
	private $userName;

	public function __construct(){
		
	}

	public function __set($name,$value){

		$this->userName = $value;

	}

	public function __get($name){

		return $this->{$name};
	}
	// public function getValue(){
	// 	return $this->userName;
	// }
}

class Admin extends User{

	public function __construct(){}

	public function ofexpressYourRole(){
		return "Admin";
	}

	public function sayHello(){
		return "Hello Admin, {$this->userName}";
	}
}


// $user = new User();
// $user->userName = "Boy";
// echo $user->userName;

$admin1 = new Admin();
$admin1->userName = "Balthazar";
echo $admin1->sayHello();
