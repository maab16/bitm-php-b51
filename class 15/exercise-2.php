<?php

class Car
{
	public $tank;

	public function fill($value){
		$this->tank = $value;
		return $this;
	}

	public function ride($miles){
		$lostFuel = $miles/50;
		$this->tank -= $lostFuel;
		return $this;
	}
}

$car = new Car();