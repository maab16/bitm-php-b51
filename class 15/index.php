<?php

class User
{
	private $userName;

	public function setUserName($property,$value){
		$this->{$property} = $value;
	}

	public function getUserName($property="userName"){
		return $this->{$property};
	}
}

class Admin extends User{

	public function __construct(){}

	public function ofexpressYourRole(){
		return "Admin";
	}

	public function sayHello(){
		return "Hello Admin, {$this->getUserName()}";
	}
}


// $user1 = new User();
// $user1->setUserName('userName','Hello');
// echo $user1->getUserName('userName');

$admin1 = new Admin();
$admin1->setUserName('userName',"Balthazar");
echo $admin1->sayHello();