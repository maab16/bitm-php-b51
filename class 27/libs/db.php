<?php

$db = new mysqli('localhost','root','','167473');

if(mysqli_connect_errno()){
	printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

function sanitize($data){
	return  $db->real_escape_string($data);
}