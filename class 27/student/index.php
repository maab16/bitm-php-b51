<?php

require_once '../libs/db.php';

if(isset($_POST['create_student'])){
	
	$first_name = (isset($_POST['first_name'])) ? htmlspecialchars($_POST['first_name']) : '';
	$last_name = (isset($_POST['last_name'])) ? htmlspecialchars($_POST['last_name']) : '';
	$seip_id = (isset($_POST['seip_id'])) ? htmlspecialchars($_POST['seip_id']) : '';
	$date = date('Y-m-d H:i:s');
	$select_sql = "SELECT COUNT(seip_id) FROM students WHERE seip_id = ?";
	$select = $db->prepare($select_sql);
	$select->bind_param('i',$seip_id);
	$select->execute();
	$select->bind_result($seip_id);
	$select->fetch();
	if ($seip_id > 0) {
		# code...
	}
	if ($stmt = $db->prepare("INSERT INTO students (seip_id,first_name,last_name,created_at,updated_at) VALUES (?,?,?,?,?)")) {
		$stmt->bind_param('issdd',$seip_id,$first_name,$last_name,$date,$date);
		$stmt->execute();
	}
}