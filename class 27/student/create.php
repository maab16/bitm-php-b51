<!DOCTYPE html>
<html lang="xyz">
<head>
	<meta charset="UTF-8">
	<title>SEIP</title>
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
</head>

<body>
	<div class="container">
		<form action="index.php" method="post" class="student_form">
			<div class="form-group">
				<label for="first_name">First Name :</label>
				<input type="text" name="first_name" id="first_name" class="first_name form-control" placeholder="Enter Your First Name">
			</div>
			<div class="form-group">
				<label for="last_name">Last Name :</label>
				<input type="text" name="last_name" id="last_name" class="last_name form-control" placeholder="Enter Your Last Name">
			</div>
			<div class="form-group">
				<label for="seip_id">SEIP ID :</label>
				<input type="number" name="seip_id" id="seip_id" class="seip_id form-control" placeholder="Enter Your SEIP ID">
			</div>
			<div class="form-group">
				<input type="submit" name="create_student" id="submit" class="submit btn btn-primary" value="Submit">
			</div>
		</form>
	</div>
</body>

</html>