<!DOCTYPE html>
<html lang="xyz">
<head>
	<meta charset="UTF-8">
	<title>SEIP</title>
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
</head>

<body>
	<div class="container">
		<form action="index.php" method="post" class="student_form">
			<div class="form-group">
				<label for="course_code">Course Code :</label>
				<input type="number" name="course_code" id="course_code" class="course_code form-control" placeholder="Enter Course Code">
			</div>
			<div class="form-group">
				<label for="course_title">Course Title :</label>
				<input type="text" name="course_title" id="course_title" class="course_title form-control" placeholder="Enter Course Title">
			</div>
			<div class="form-group">
				<input type="submit" name="create_course" id="submit" class="submit btn btn-primary" value="Submit">
			</div>
		</form>
	</div>
</body>

</html>