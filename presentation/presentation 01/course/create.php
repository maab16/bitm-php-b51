    <?php require_once('../header.php');?>
    <!-- Start Main -->
    <main class="main">
      <!-- Start Sidebar Menu -->
      <?php require_once('../aside.php');?>
      <!--End Sidebar Menu-->
      <!-- Start Content -->
      <section class="content-container">
        <h2 class="title dashboard-title">Create Courses</h2>
        <div class="content">
          <form action="libs/create_course.php" method="post" enctype="multipart/form-data" class="form">
            <div class="form-group">
              <label for="course_code">Course Code :</label>
              <input type="number" name="code" id="course_code" class="course_code form-control" placeholder="Enter Course Code">
            </div>
            <div class="form-group">
              <label for="course_title">Course Title :</label>
              <input type="text" name="title" id="course_title" class="course_title form-control" placeholder="Enter Course Title">
            </div>
            <div class="form-group">
              <input type="submit" name="create_course" id="submit" class="submit btn btn-success" value="Create Course">
            </div>
          </form>
        </div>
      </section><!--End Content-->
    </main>
    <?php require_once('../footer.php');?>