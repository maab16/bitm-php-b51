<aside class="sidebar-menu">
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a href="index.php">Dashboard</a>
        </h4>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Students</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <ul class="sub-menu">
          <li><a href="student">All Students</a></li>
          <li><a href="student/create.php">Create Student</a></li>
        </ul>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Courses</a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <ul class="sub-menu">
          <li><a href="course">All Courses</a></li>
          <li><a href="course/create.php">Create Course</a></li>
        </ul>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a href="assign">Assign Course</a>
        </h4>
      </div>
    </div>
  </div>
</aside>