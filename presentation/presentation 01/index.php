    <?php require_once('header.php');?>
    <!-- Start Main -->
    <main class="main">
      <!-- Start Sidebar Menu -->
      <?php require_once('aside.php');?>
      <!--End Sidebar Menu-->
      <!-- Start Content -->
      <section class="content-container">
        <h2 class="title dashboard-title">Dashboard</h2>
        <div class="content">
          <div class="row">
            <!-- Start Category Item 1-->
            <div class="col-sm-4 col-md-3">
              <div class="category-item">
                <!-- Start Category Top-->
                <div class="category-item-top">
                  <div class="category-item-logo">
                    <i class="fa fa-users" aria-hidden="true"></i>
                  </div>
                  <div class="category-item-content">
                    <span class="total-students">20</span>
                    <h3>Students</h3>
                  </div>
                </div><!-- End Category Top -->
                <!-- Start Category Bottom -->
                <div class="category-item-bottom">
                  <a href="student/index.php">View All</a>
                  <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                </div><!-- End Category Bottom -->
              </div>
            </div><!-- End Category Item 1 -->
            <!-- Start Category Item 2 -->
            <div class="col-sm-4 col-md-3">
              <div class="category-item course-category">
                <!-- Start Category Top -->
                <div class="category-item-top">
                  <div class="category-item-logo">
                    <i class="fa fa-book" aria-hidden="true"></i>
                  </div>
                  <div class="category-item-content">
                    <span class="total-students">10</span>
                    <h3>Corses</h3>
                  </div>
                </div><!-- End Category Top -->
                <!-- Start Category Bottom -->
                <div class="category-item-bottom">
                  <a href="course/index.php">View All</a>
                  <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                </div><!-- End Category Bottom -->
              </div>
            </div><!-- End Category Item 2 -->
            <!-- Start Category Item 3-->
            <div class="col-sm-4 col-md-3">
              <div class="category-item class-category">
                <!-- Start Category Top -->
                <div class="category-item-top">
                  <div class="category-item-logo">
                    <i class="fa fa-users" aria-hidden="true"></i>
                  </div>
                  <div class="category-item-content">
                    <span class="total-students">40</span>
                    <h3>Assign</h3>
                  </div>
                </div><!-- End Category Top -->
                <!-- Start Category Bottom -->
                <div class="category-item-bottom">
                  <a href="assign/index.php">View All</a>
                  <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                </div><!-- End Category Bottom -->
              </div>
            </div><!-- End Category Item 3 -->
            <!-- Start Category Item 4 -->
            <div class="col-sm-4 col-md-3">
              <div class="category-item test-category">
                <!-- Start Category Top-->
                <div class="category-item-top">
                  <div class="category-item-logo">
                    <i class="fa fa-users" aria-hidden="true"></i>
                  </div>
                  <div class="category-item-content">
                    <span class="total-students">15</span>
                    <h3>Tests</h3>
                  </div>
                </div><!-- End Category Top -->
                <!-- Start Category Bottom -->
                <div class="category-item-bottom">
                  <a href="#">View All</a>
                  <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                </div><!-- End Category Bottom-->
              </div>
            </div><!-- End Category Item 4 -->
          </div><!-- End Category Items -->
        </div>
      </section><!--End Content-->
    </main>
    <?php require_once('footer.php');?>