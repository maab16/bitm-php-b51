    <?php require_once('../header.php');?>
    <!-- Start Main -->
    <main class="main">
      <!-- Start Sidebar Menu -->
      <?php require_once('../aside.php');?>
      <!--End Sidebar Menu-->
      <!-- Start Content -->
      <section class="content-container">
        <h2 class="title dashboard-title">Assign Student</h2>
        <div class="content">
          <form action="libs/assign_course.php" method="post" class="form">
            <div class="form-group">
              <select name="student" class="student form-control">
                <option value="">Select Student</option>
                <option value="1">A</option>
                <option value="2">B</option>
                <option value="3">C</option>
                <option value="4">D</option>
              </select>
            </div>
            <div class="form-group">
              <select name="student" class="student form-control">
                <option value="">Select Course</option>
                <option value="1">PHP</option>
                <option value="2">HTML</option>
                <option value="3">CSS</option>
                <option value="4">JS</option>
              </select>
            </div>
            <div class="form-group">
              <input type="submit" name="assign_course" id="assign_course" class="assign_course btn btn-success" value="Assign Course">
            </div>
          </form>
        </div>
      </section><!--End Content-->
    </main>
    <?php require_once('../footer.php');?>