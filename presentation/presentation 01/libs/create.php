<?php

require_once '../libs/db.php';

if(isset($_POST['create_student'])){
	
	$first_name = (isset($_POST['first_name'])) ? htmlspecialchars($_POST['first_name']) : '';
	$last_name = (isset($_POST['last_name'])) ? htmlspecialchars($_POST['last_name']) : '';
	$seip_id = (isset($_POST['seip_id'])) ? (int)htmlspecialchars($_POST['seip_id']) : '';
	$date = date('Y-m-d H:i:s');

	if(!checkExists('students',array('seip_id'=>$seip_id))){
		$insert = insert(
					'students',
					array(
					'first_name',
					'last_name',
					'seip_id',
					'created_at',
					'updated_at'
					),
					array(
						$first_name,
						$last_name,
						$seip_id,
						$date,
						$date
					)
				);
		if ($insert) {
			echo "Inser Successfully";
		}
	}else{
		echo "Student Already exists";
	}
}