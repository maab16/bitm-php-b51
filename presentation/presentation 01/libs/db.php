<?php

function connectDB(){
	$db = new mysqli('localhost','root','','student_management');

	if(mysqli_connect_errno()){
		printf("Connect failed: %s\n", mysqli_connect_error());
	    exit();
	}
	return $db;
}

function sanitize($data){
	return  $db->real_escape_string($data);
}

function insert($tableName,$fields,$values){
	$sql_fields = '';
	/*Table*/
	if (!empty($tableName)) {
		$table = $tableName;
	}
	/*Fields*/
	if (is_array($fields)) {
		$i = 1;
		foreach ($fields as $field) {
			$sql_fields .= $field;
			if ($i < count($fields)) {
				$sql_fields .= ',';
			}
			$i++;
		}
	}
	/*Values*/
	if (is_array($values)) {
		$bind_ques = '';
		$bind_types = '';
		$field_values = array();
		$i = 1;
		foreach ($values as $key => $value) {
			$field_values[] = &$values[$key];
			$bind_ques .= '?';
			if ($i < count($values)) {
				$bind_ques .= ',';
			}
			$i++;
		}
	}

	$sql = "INSERT INTO {$table} ({$sql_fields}) VALUES ({$bind_ques})";

	$types =  getTypes($values);
	array_unshift($field_values,$types);

	$db = connectDB();

	if ($stmt = $db->prepare($sql)) {
		call_user_func_array(array($stmt, 'bind_param'), $field_values);
		if($stmt->execute()){
			return true;
		}
	}

	return false;
}

function getAllData($tableName,$from,$where){
	$fields = "";
	$values = array();
	$froms = "";
	if (empty($tableName)) {
		throw new Exception("Table name must be not empty", 1);
		
	}
	if (!empty($from)) {
		$i = 1;
		foreach ($from as $value) {
			$froms .= $value;
			if($i < count($from)){
				$froms .= ",";
			}
			$i++;
		}
	}else{
		$froms = '*';
	}
	
	if (is_array($where)) {
		$i = 1;
		foreach ($where as $key => $value) {
			$fields .= $key;
			$values[]= &$where[$key];
			if ($i < count($where)) {
				$fields .= 'AND';
			}
		}
	}
	
	$query = "SELECT {$froms} FROM {$tableName} WHERE {$fields} = ?"; 
	
	$db = connectDB();
	if($stmt = $db->prepare($query)){
	    $types =  getTypes($values);
		array_unshift($values,$types);

	    call_user_func_array(array($stmt, 'bind_param'), $values);

	   /* execute query */
	   $stmt->execute();

	   /* Get the result */
	   $result = $stmt->get_result();

	   /* Get the number of rows */
	   $num_of_rows = $result->num_rows;

	   $row = $result->fetch_object();
	   print_r($row);
	}
}
function checkExists($tableName,$where){
	$fields = "";
	$values = array();
	if (empty($tableName)) {
		throw new Exception("Table name must be not empty", 1);
		
	}
	
	if (is_array($where)) {
		$i = 1;
		foreach ($where as $key => $value) {
			$fields .= $key;
			$values[]= &$where[$key];
			if ($i < count($where)) {
				$fields .= 'AND';
			}
		}
	}
	
	$query = "SELECT * FROM {$tableName} WHERE {$fields} = ?"; 

	$db = connectDB();
	if($stmt = $db->prepare($query)){
	   
	    $types =  getTypes($values);
		array_unshift($values,$types);

	    call_user_func_array(array($stmt, 'bind_param'), $values);

	   /* execute query */
	   if($stmt->execute()){
		   	/* Get the result */
		   $result = $stmt->get_result();

		   /* Get the number of rows */
		   if($result->num_rows > 0){
		   	return true;
		   }
	   }

	   return false;
	   
	}
}
function getTypes($values){
	$types = "";
	foreach ($values as $key => $value) {
		switch ($value) {
			case is_int($value):
				$types .= 'i';
				break;
			case is_double($value):
				$types .= 'd';
				break;
			case is_string($value):
				$types .= 's';
				break;
			case is_bool($value):
				$types .= 'b';
				break;
		}
	}

	return $types;
}
