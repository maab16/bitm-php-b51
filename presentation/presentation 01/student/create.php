    <?php require_once('../header.php');?>
    <!-- Start Main -->
    <main class="main">
      <!-- Start Sidebar Menu -->
      <?php require_once('../aside.php');?>
      <!--End Sidebar Menu-->
      <!-- Start Content -->
      <section class="content-container">
        <h2 class="title dashboard-title">Create Student</h2>
        <div class="content">
          <form action="libs/create.php" method="post" enctype="multipart/form-data" class="form">
            <div class="form-group">
              <label for="first_name">First Name :</label>
              <input type="text" name="first_name" id="first_name" class="first_name form-control" placeholder="Enter Your First Name">
            </div>
            <div class="form-group">
              <label for="last_name">First Name :</label>
              <input type="text" name="last_name" id="last_name" class="last_name form-control" placeholder="Enter Your Last Name">
            </div>
            <div class="form-group">
              <label for="seip_id">First Name :</label>
              <input type="number" name="seip_id" id="seip_id" class="seip_id form-control" placeholder="Enter Your SEIP ID">
            </div>
            <div class="form-group">
              <input type="submit" name="create_student" id="submit" class="submit btn btn-success" value="Create Student">
            </div>
          </form>
        </div>
      </section><!--End Content-->
    </main>
    <?php require_once('../footer.php');?>