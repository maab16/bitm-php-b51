$(function() {
	'use strict';
	/*Banner*/
	// var movementStrength = 25;
	// var height = movementStrength / $(window).height();
	// var width = movementStrength / $(window).width();
	// $("#banner").mousemove(function(e){
 //      	var pageX = e.pageX - ($(window).width() / 2);
 //      	var pageY = e.pageY - ($(window).height() / 2);
 //      	var newvalueX = width * pageX * -1 - 25;
 //      	var newvalueY = height * pageY * -1 - 50;
 //      	$(this).css("background-position", newvalueX+"px     "+newvalueY+"px");
	// });
	/*
	 ===========================================
	 			Masonry
	 ===========================================
	*/
	var self = $("#masonry-portfolio");

	self.imagesLoaded(function () {
	    self.masonry({
	        gutterWidth: 15,
	        isAnimated: true,
	        itemSelector: ".portfolio"
	    });
	});

	$(".portfolio-controllers button").on('click',function(e) {
	    e.preventDefault();
	    $(".portfolio-controllers button").removeClass('active-work');
	    $(this).addClass('active-work');
	    var filter = $(this).attr("data-filter");

	    self.masonryFilter({
	        filter: function () {
	            if (!filter) return true;
	            return $(this).attr("data-filter") == filter;
	        }
	    });
	});
	/*
	 ===========================================
	 					Portfolio
	 ===========================================
	*/
	$('.products').filterData({
		aspectRatio: '1:1',
		responsive : [
            {
                breakpoint : 1200,
                containerWidth : 1170,
                settings : {
                    nOfRow : 3,
                    nOfColumn : 4
                }
            },
            {
                breakpoint : 992,
                containerWidth : 970,
                settings : {
                    nOfRow : 3,
                    nOfColumn : 4
                }
            },
            {
                breakpoint : 768,
                containerWidth : 750,
                settings : {
                    nOfRow : 2,
                    nOfColumn : 2
                }
            }
        ]
	});
	$('.product-controllers button').on('click',function(){
		$('.product-controllers button').removeClass('active-work');
		$(this).addClass('active-work');
	});
});
