<?php
require_once 'shape_abstract.php';

class TryAngle extends Shape
{
	private $h_axe;
	private $v_axe;

	public function getArea(){
		return 0.5 * $this->h_axe * $this->v_axe;
	}
	public function __SET($property,$value){
		$this->$property = $value;
	}
	public function __GET($property){
		return $this->$property;
	}
}