<?php

interface Car{

	public function drive();
	public function fillGas($fillAmount);
}