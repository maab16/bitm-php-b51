<?php

require_once 'car_interface.php';

class BMW implements Car
{
	private $fill = 0;
	public $miles;
	public $gas;
	public function drive(){
		$this->gas = $this->fill - $this->miles*2;
		return $this;
	}
	public function fillGas($fillAmount){
		$this->fill += $fillAmount;
		return $this;
	}

	public function __SET($property,$value){
		$this->$property = $value;
		return $this;
	}

	public function __GET($property){
		return $this->$property;
	}
}