<?php

require_once 'bmw.php';
require_once 'tryangle.php';

// Car
$car1 = new BMW();
$car1->miles = 5;
$car1->fillGas(200)->drive();
echo "Fill Amount : ".$car1->fill."<br>";
echo "Gas : ".$car1->gas."<br>";

// Shape
$circle = new TryAngle();

$circle->h_axe = 15;
$circle->v_axe = 25;

echo "The TryAngle area of {$circle->h_axe} and {$circle->v_axe} is ".$circle->getArea();